package com.example.activitydatapass.put_extra

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.activitydatapass.R
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_parcel.*

class ActivityHome : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        btnSend.setOnClickListener {

            var userName = username.text.toString()
            var password = password.text.toString()

            val detail = Intent(this, ActivityDetail::class.java)
            detail.putExtra("username", userName)
            detail.putExtra("password", password)
            startActivity(detail)
        }

        username.isSingleLine = true
        password.isSingleLine = true
    }
}
