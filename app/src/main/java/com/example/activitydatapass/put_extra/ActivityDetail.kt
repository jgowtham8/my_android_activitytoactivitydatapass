package com.example.activitydatapass.put_extra

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.activitydatapass.R
import kotlinx.android.synthetic.main.activity_detail.*

class ActivityDetail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)


        val username: String? = intent.getStringExtra("username")
        val password: String? = intent.getStringExtra("password")

        tvUser.text = username
        tvPass.text = password
    }
}
