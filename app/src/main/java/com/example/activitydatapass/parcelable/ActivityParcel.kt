package com.example.activitydatapass.parcelable

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.activitydatapass.R
import com.example.activitydatapass.data_classes.Student
import kotlinx.android.synthetic.main.activity_parcel.*

class ActivityParcel : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parcel)

        btnSend2.setOnClickListener {
            val name = name.text.toString()
            val age = age.text.toString()
            val address = address.text.toString()
            val job = job.text.toString()

            val student = Student(name,age.toIntOrNull()!!,address,job)
            val actPar = Intent(this, ActivityParcelDetail::class.java)
            val bundle = Bundle()
            bundle.putParcelable("key", student)
            actPar.putExtra("studentData", bundle)
            startActivity(actPar)
        }

        name.isSingleLine = true
        age.isSingleLine = true
        address.isSingleLine = true
        job.isSingleLine = true


    }
}
