package com.example.activitydatapass.parcelable

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.activitydatapass.R
import com.example.activitydatapass.data_classes.Student
import kotlinx.android.synthetic.main.activity_parcel.*
import kotlinx.android.synthetic.main.activity_parcel.name
import kotlinx.android.synthetic.main.activity_parcel_detail.*
import java.util.*

class ActivityParcelDetail : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parcel_detail)

        val stu = intent.getBundleExtra("studentData")
        val obj = stu.getParcelable<Student>("key")

        name1.text = obj!!.name
        age1.text = obj!!.age.toString()
        address1.text = obj!!.address
        job1.text = obj!!.job
    }
}
