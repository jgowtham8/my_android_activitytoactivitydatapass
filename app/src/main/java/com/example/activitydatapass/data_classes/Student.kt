package com.example.activitydatapass.data_classes

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Student(val name_:String, val age_:Int,val address_:String, val job_:String) : Parcelable{
    var name: String = name_
    var age: Int = age_
    var address: String = address_
    var job: String = job_
}