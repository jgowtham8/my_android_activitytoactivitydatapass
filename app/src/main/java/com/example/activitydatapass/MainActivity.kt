package com.example.activitydatapass

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.activitydatapass.parcelable.ActivityParcel
import com.example.activitydatapass.put_extra.ActivityHome
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnPutExtra.setOnClickListener {
            val home = Intent(this, ActivityHome::class.java)
            startActivity(home)
        }

        btnParcelable.setOnClickListener {
            val parcel = Intent(this, ActivityParcel::class.java)
            startActivity(parcel)
        }
    }
}
